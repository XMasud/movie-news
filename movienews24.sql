-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2018 at 05:35 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movienews24`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title` varchar(10000) NOT NULL,
  `blog` text NOT NULL,
  `picture` varchar(1000) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `author_name` varchar(100) NOT NULL,
  `auth_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `blog`, `picture`, `datetime`, `author_name`, `auth_id`) VALUES
(5, 'An Introduction', 'dffferfrf', '598b26e3be5218.25817215.png', '0000-00-00 00:00:00', '', 2),
(13, 'AIUB', 'The case of AIUB on IQA was presented during the Thematic Interactive Sessions and in the Exhibits showcasing the good practices of the QA in Asia-Paciifc. A Newsletter: The maiden issue on QA entitled THE VANGUARD .Copies were displayed and to the  best interest of the participants, they picked up copies. There were many organizations / institutions exhibited their practices through variety of documents. AIUBâ€™s Videoâ€™s presentation on the Journey to QA was presented during the parallel  session. I informed the participants  that AIUB was chosen as The Best Model QA University by APQN last year during its international conference in Fiji. Our experience in QA is now brought to international landscape in this conference. AIUB will continue to search for avenues where we can learn more strategies to strengthen and make QA functional in the university.', '63519-WSN.png', '2017-08-17 21:30:52', '', 2),
(14, 'hgvb', 'bvbv', '598b4b743c7192.75844097.png', '2017-08-17 02:35:27', '', 2),
(15, 'Learning Aid using LED Matrix', 'fdsfd', '598b509b71a4d3.30624195.png', '2017-08-16 04:30:03', '', 2),
(17, 'MAsiud', 'dcd', '598b558fd68835.20477121.docx', '2017-07-31 19:00:37', '', 2),
(18, 'Masud', 'cdscdsc', '598b5e04078296.12275802.jpg', '2017-08-12 03:10:27', 'AIUB EX-Student', 2),
(19, 'New Bangla Flim is Coming !!!!', 'In JavaScript, PHP, and ASP there are functions that can be used to URL encode a string.\r\n\r\nIn JavaScript you can use the encodeURI() function.\r\n\r\nPHP has the rawurlencode() function, and ASP has the Server.URLEncode() function.\r\n\r\nClick the "URL Encode" button to see how the JavaScript function encodes the text.', '598b759035aad3.18904933.jpg', '2017-08-17 21:30:29', 'AIUB-PC', 2);

-- --------------------------------------------------------

--
-- Table structure for table `blogs_comment`
--

CREATE TABLE `blogs_comment` (
  `id` int(10) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `blogs_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs_comment`
--

INSERT INTO `blogs_comment` (`id`, `comment`, `blogs_id`, `user_id`, `date`) VALUES
(1, 'Hey', 19, 108, '2017-08-13 17:08:00'),
(2, 'Hi', 19, 108, '2017-08-08 18:00:00'),
(3, 'Masud', 19, 108, '2017-08-13 17:08:00'),
(4, 'AA', 19, 108, '2017-08-13 17:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `editorpicks`
--

CREATE TABLE `editorpicks` (
  `id` int(11) NOT NULL,
  `title` varchar(10000) NOT NULL,
  `editorpick` text NOT NULL,
  `picture` varchar(1000) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `auth_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `editorpicks`
--

INSERT INTO `editorpicks` (`id`, `title`, `editorpick`, `picture`, `datetime`, `auth_id`) VALUES
(1, 'Microprocessor ', 'Edit Ready', '16742-Editorpik1.jpg', '2017-07-31 19:05:06', 2),
(3, 'Learning Aid using LED Matrix', 'BBBBB', '52869-banner_sample2.jpg', '2017-08-01 02:15:09', 2),
(4, 'An Introduction to Infrared Technology: Applications in the Home, Classroom, Workplace, and Beyond', 'AIUB', '598dc9192e25f5.38742363.png', '2017-08-22 22:30:59', 2);

-- --------------------------------------------------------

--
-- Table structure for table `editorpicks_comment`
--

CREATE TABLE `editorpicks_comment` (
  `id` int(10) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `editorpick_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `editorpicks_comment`
--

INSERT INTO `editorpicks_comment` (`id`, `comment`, `editorpick_id`, `user_id`, `date`) VALUES
(1, 'gfffgfgfgfg', 6, 108, '2017-08-13 16:08:00'),
(2, 'Rana Well Done', 6, 108, '2017-08-13 16:08:00'),
(3, 'babu', 6, 108, '2017-08-13 16:08:00'),
(4, 'The Science Fair organized by BAF Shaheen College held last 2-3 August 2017 was a resounding success in spite of the rain on the second and last day of the two-day event. Ninety two (92) colleges and schools participated in the science fair and various competitions like Olympiad in Physics, Chemistry, Biology, Mathematics, IT, Extemporaneous Speaking, and others were held.', 6, 108, '2017-08-13 16:08:00'),
(5, 'The Science Fair organized by BAF Shaheen College held last 2-3 August 2017 was a resounding success in spite of the rain on the second and last day of the two-day event. Ninety two (92) colleges and schools participated in the science fair and various competitions like Olympiad in Physics, Chemistry, Biology, Mathematics, IT, Extemporaneous Speaking, and others were held.', 6, 108, '2017-08-13 16:08:00'),
(6, 'fcdfd', 6, 108, '2017-08-13 17:08:00'),
(7, 'The Principal, likewise, delivered a message during the closing program. He expressed  thanks and appreciation to all the participants, congratulate the winners, the organizers and the number of sponsors who shared their resources which made the event possible and successful. As a major sponsor, AIUB, was given appropriate recognition and looking forward to be a partner again in next yearâ€™s event. The Principal encouraged also the students to enroll in AIUB for their university degree. This call was received by the students with enthusiasm  a big hand.', 6, 108, '2017-08-13 17:08:00'),
(8, 'The Principal, likewise, delivered a message during the closing program. He expressed  thanks and appreciation to all the participants, congratulate the winners, the organizers and the number of sponsors who shared their resources which made the event possible and successful. As a major sponsor, AIUB, was given appropriate recognition and looking forward to be a partner again in next yearâ€™s event. The Principal encouraged also the students to enroll in AIUB for their university degree. This call was received by the students with enthusiasm  a big hand.', 6, 108, '2017-08-13 17:08:00'),
(9, 'hey', 6, 108, '2017-08-13 17:08:00'),
(10, 'oooooooooooooooooooooooooooooooo', 6, 108, '2017-08-13 17:08:00'),
(11, 'dsds', 6, 108, '2017-08-13 17:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` int(11) NOT NULL,
  `moviename` varchar(100) NOT NULL,
  `genre` varchar(700) NOT NULL,
  `type` varchar(10) NOT NULL,
  `releaseDate` varchar(12) NOT NULL,
  `country` varchar(20) NOT NULL,
  `shortDescription` varchar(20000) NOT NULL,
  `director` varchar(200) NOT NULL,
  `musicComposer` varchar(200) NOT NULL,
  `cast` varchar(10000) NOT NULL,
  `storyline` text NOT NULL,
  `picture` varchar(1000) NOT NULL,
  `auth_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `moviename`, `genre`, `type`, `releaseDate`, `country`, `shortDescription`, `director`, `musicComposer`, `cast`, `storyline`, `picture`, `auth_id`) VALUES
(2, 'XXX Return', 'Hollywod', 'Action', '1915-02-01', 'USA', 'Ninja fightfskjvhffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff', 'James Cameron', 'Marvel Studio', 'Kesha', 'ase kisu', '20252-Figure-1-WSN-architecture.png', 2),
(3, 'Dhaka Attack', 'Bangla Cinema', 'Action Ban', '1916-02-01', 'Bangladesh', 'Action flimfsdsssssssssssssssssssssssssssssdcdsdcssdcsdscsdsssdsssssssssssssssssssssssssss', 'PK Roy Ch.', 'AR Rahman India', 'Jhoney Dep', 'Agkhdfg vjfhnvjf vjnfiojv vnhjfivj vcnfk', '82001-donald.PNG', 2),
(4, 'War of flower', 'Romantic', 'Romantic', '2017-08-24', 'USA', 'jlksdjkfdhfjkd cvjhndiovjdoi coidjcvdjvcdio mnvdf9', 'ZZZZZZZZZ', 'QQQQQQQQQQ', 'AWfgdgfgfg', 'gfgfgfgfg', '599079a8387eb1.88708022.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `newcomer`
--

CREATE TABLE `newcomer` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `birth_date` varchar(30) NOT NULL,
  `language` text NOT NULL,
  `height` text NOT NULL,
  `weight` text NOT NULL,
  `color` text NOT NULL,
  `religion` text NOT NULL,
  `marital_status` text NOT NULL,
  `gender` text NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `facebook` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `permanent_address` varchar(500) NOT NULL,
  `present_address` varchar(500) NOT NULL,
  `high_school` varchar(500) NOT NULL,
  `high_school_pass` int(20) NOT NULL,
  `college` varchar(500) NOT NULL,
  `college_pass` int(20) NOT NULL,
  `honors` varchar(500) NOT NULL,
  `honors_pass` int(20) NOT NULL,
  `masters` varchar(500) NOT NULL,
  `another` varchar(500) NOT NULL,
  `fathers_name` text NOT NULL,
  `mothers_name` text NOT NULL,
  `fathers_occupation` text NOT NULL,
  `mathers_occupation` text NOT NULL,
  `sibling` varchar(200) NOT NULL,
  `total_member` int(20) NOT NULL,
  `hobby` varchar(200) NOT NULL,
  `interested` varchar(200) NOT NULL,
  `habit` varchar(200) NOT NULL,
  `photo` varchar(500) NOT NULL,
  `audio` varchar(500) NOT NULL,
  `video` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newcomer`
--

INSERT INTO `newcomer` (`id`, `name`, `birth_date`, `language`, `height`, `weight`, `color`, `religion`, `marital_status`, `gender`, `phone_number`, `facebook`, `email`, `permanent_address`, `present_address`, `high_school`, `high_school_pass`, `college`, `college_pass`, `honors`, `honors_pass`, `masters`, `another`, `fathers_name`, `mothers_name`, `fathers_occupation`, `mathers_occupation`, `sibling`, `total_member`, `hobby`, `interested`, `habit`, `photo`, `audio`, `video`) VALUES
(1, 'wwww', 'w', 'w', 'w', 'w', 'w', 'w', 'Married', 'Male', 'w', 'w', 'sdsd@d.com', 'ds', 'dfs', 'ds', 0, 'ds', 0, 'ds', 0, 'sd', 'sd', 'ds', 'sd', 'sd', 'sd', 'sd', 0, 'sd', 'sd', 'sd', '5acdf75a0934b9.30291206.jpg', '01-Hello.mp3', 'IMG_2688.MOV'),
(2, 'Masud Rana', '1/7/1994', 'English', '510', '81', 'White', 'Islam', 'Unmarried', 'Male', '0123545654', 'https://www.facebook.com/', 'ms@sfs.com', 'Gulshan - 1, Dhaka - 1211', 'Gulshan - 1, Dhaka - 1211', 'DSK School', 2010, 'Shaheen College', 2012, 'AIUB', 2018, 'Not Applicable', 'Not Applicable', 'M. M Khan', 'House Wife', 'R. R Khan', 'Service Holder', '2', 4, 'Sleeping', 'Coding', 'Sleeping', '5acf195cd042e8.16489452.jpg', '5acf195cd0a380.65140806.mp3', '5acf195cd07756.63749064.mp4'),
(4, 'Md.Masud Rana', 'q', 'esd', '43', '34', '34', '34', 'Married', 'Male', '1939601509', '43', 'masud.aiub.cse@gmail.com', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', '4343', 4343, '3443', 4343, '4343', 4343, '4343', '3443', 'wsds', 'ds', 'sd', 'ds', 'ds', 0, 'ds', 'ds', 'ds', '5acf1c5f79ee14.37453567.png', '5acf1c5f7a4160.99117149.mp4', '5acf1c5f7a1997.64069553.mp4'),
(5, 'Md.Masud Rana', '1/7/1994', 'ds', 'ds', 'ds', 'ds', 'ds', 'Married', 'Male', '1939601509d', 'https://www.facebook.com/', 'masud.aidub.cse@gmail.com', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', 'ds', 0, 'ds', 0, 'dsds', 0, 'dsds', 'ds', 'sd', 'sd', 'sd', 'sd', 'sd', 0, 'sd', 'sd', 'sdsdsd', '5acf1f6f880c23.88311513.jpg', '', '5acf1f6f88cd10.35024257.mp4'),
(6, 'Md.Masud Rana', 'dfs', 'fddf', 'dfdf', 'fd', 'df', 'df', 'Married', 'Male', '1939601509', 'dff', 'masud.aiub.cse@dgmail.com', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', 'df', 0, 'df', 0, 'df', 0, 'df', 'df', 'fd', 'df', 'df', 'df', 'df', 0, 'df', 'df', 'df', '5acf20618fece9.87181525.jpg', 'ee.mp3', '5acf2061903952.17786335.mp4'),
(10, 'Md.Masud Rana', 'fd', 'fd', 'fd', 'df', 'df', 'df', 'Married', 'Male', '1939601509', 'df', 'masud.aiub.cse@gmail.com', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', 'dfdf', 0, 'df', 0, 'df', 0, 'df', 'df', 'df', 're', 'df', 'rer', 'rer', 0, 're', 'er', 're', '5acf25ad900fb5.35664779.png', '5acf25ad906728.69147263.png', '5acf25ad903e99.24221103.png'),
(11, 'Razib', '1/7/1994', 'English', '43', '34', 'q', 'Islam', 'Married', 'Male', '1939601509', 'https://www.facebook.com/', 'masud.aiub.cse@gmail.com', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', 'Plot-480/4,Holding No-188/4,Khilgaon Tilpapara', 'DSK School', 201, 'df', 0, 'df', 0, 'df', '2', 'M. M Khan', 'House Wife', 'R. R Khan', 'Service Holder', 'Md.Masud Rana', 2, 'Sleeping', 'Coding', '2', '5acf7abe81b6d6.08276570.png', '5acf7abe873af9.42416333.mp3', '5acf7abe86a781.29389849.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(10000) NOT NULL,
  `news` text NOT NULL,
  `picture` varchar(1000) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `auth_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `news`, `picture`, `datetime`, `auth_id`) VALUES
(2, 'MEMORANDUM', 'It is to notify all concerned that the University will remain closed on Monday.', '22007-banner_sample6.jpg', '2017-07-31 19:00:38', 2),
(3, 'MARKETING DEPARTMENTâ€™S ACADEMIC VISIT TO KAZI FOOD INDUSTRIES LIMITED', 'The Department of Marketing, Faculty of Business Administration, American International University-Bangladesh has organized an academic visit to Kazi Food Industries Limited (KFIL), Sarker Market, Ashulia, Savar, Dhaka on May 3, 2017 (Wednesday). This visit was arranged as a part of the content development process of the two newly launched courses â€œAgro-based Product Marketingâ€ and â€œProduct Innovation and Managementâ€. It is to be noted that these two new courses were developed and offered to the Undergraduate Program of the university as a joint effort of AIUB and KATALYST. The visiting team was headed by Prof. Dr. Charles C. Villanueva, Pro-Vice Chancellor, Dean, Faculty of Business Administration, and also the Project Director, Mr. Stanley Rodrick, Mr. Hamidul Islam, Dr. Sahin Akter, Ms. Tahsina Khan, Assistant Professors, and Mr. Saihad Sahid Rahman, Lecturer, from the Department of Marketing.', '35183-s2.png', '2017-07-31 19:00:09', 2);

-- --------------------------------------------------------

--
-- Table structure for table `news_comment`
--

CREATE TABLE `news_comment` (
  `id` int(10) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `news_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_comment`
--

INSERT INTO `news_comment` (`id`, `comment`, `news_id`, `user_id`, `date`) VALUES
(1, 'dwdfsedfsff', 3, 108, '2017-08-13 14:08:00'),
(2, 'dfdffdfd', 2, 108, '2017-08-13 14:08:00'),
(3, 'gfg', 2, 108, '2017-08-13 14:08:00'),
(4, 'sdsxc', 2, 108, '2017-08-13 14:08:00'),
(5, 'sdfs', 2, 108, '2017-08-13 14:08:00'),
(6, 'Masud', 2, 108, '2017-08-13 14:08:00'),
(7, 'Now', 2, 108, '2017-08-13 14:08:00'),
(8, 'cdxcdc', 2, 108, '2017-08-13 14:08:00'),
(9, 'dcs', 2, 108, '2017-08-13 14:08:00'),
(10, 'Masud', 2, 108, '2017-08-13 14:08:00'),
(11, 'gvfgvfvfv', 2, 108, '2017-08-13 15:08:00'),
(12, 'Rana', 2, 108, '2017-08-13 15:08:00'),
(13, 'Rubel Bhai', 2, 108, '2017-08-13 16:08:00'),
(14, 'babu', 2, 108, '2017-08-13 16:08:00'),
(15, 'kama', 2, 108, '2017-08-13 16:08:00'),
(16, 'Emon ken?', 3, 108, '2017-08-13 17:08:00'),
(17, 'Valo ', 3, 108, '2017-08-13 17:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `opinion`
--

CREATE TABLE `opinion` (
  `id` int(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opinion`
--

INSERT INTO `opinion` (`id`, `name`, `email`, `description`) VALUES
(2, 'Ennvisio', 'ennvisio@gmail.com', 'Hey Whats up');

-- --------------------------------------------------------

--
-- Table structure for table `trailer`
--

CREATE TABLE `trailer` (
  `id` int(11) NOT NULL,
  `trailerImage` varchar(1000) NOT NULL,
  `trailerLink` varchar(10000) NOT NULL,
  `movie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trailer`
--

INSERT INTO `trailer` (`id`, `trailerImage`, `trailerLink`, `movie_id`) VALUES
(2, '85498-20106737_1330125547104217_8444939665398154440_n.jpg', 'https://www.youtube.com/embed/oIowZiM41kQ', 2),
(4, '91213-18518787_1250496088402837_1273457585_n.png', 'https://www.youtube.com/embed/ue80QwXMRHg', 2),
(6, '599079e60264c6.65929705.jpg', 'https://www.youtube.com/embed/cj2uhyfVNmQ', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(700) NOT NULL,
  `type` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `type`) VALUES
(1, 'shahankhanbd@gmail.com', 'heyhj', '10ed1697617fe7758b4236d5b791286c', 'admin'),
(2, 'shakil_mahmud@outlook.com', 'Shakil', '654321', 'admin'),
(34, 'gfgfg@gm.com', 'Masud', '1c1c43d8bafe7b8c6f3b5e7a845c8a07', 'admin'),
(98, 'admin@sfs.com', 'sfs_bd', 'e10adc3949ba59abbe56e057f20f883e', 'admin'),
(99, 'admin@sfs.com', 'sfs_bd', 'e10adc3949ba59abbe56e057f20f883e', 'admin'),
(100, 'admin@sfs.com', 'sfs_bd', 'e10adc3949ba59abbe56e057f20f883e', 'admin'),
(101, 'admin@sfs.com', 'sfs_bd', 'e10adc3949ba59abbe56e057f20f883e', 'admin'),
(102, 'admin@sfs.com', 'sfs_bd', 'e10adc3949ba59abbe56e057f20f883e', 'admin'),
(103, 'admin@sfs.com', 'sfs_bd', 'e10adc3949ba59abbe56e057f20f883e', 'admin'),
(106, 'masud_9015@yahoo.com', 'masud', '4545454', 'admin'),
(107, 'masud_9015@yahoo.com', 'masud', '4545454', 'admin'),
(108, 'masud_9015@yahoo.com', 'X_Masud', '123456', 'user'),
(109, 'admin@ennvisio.com', 'rana', '12345', 'user'),
(110, 'rana@ennvisio.com', 'qwe', 'e10adc3949ba59abbe56e057f20f883e', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `authorId` (`auth_id`);

--
-- Indexes for table `blogs_comment`
--
ALTER TABLE `blogs_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `editorpicks`
--
ALTER TABLE `editorpicks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `authorId` (`auth_id`);

--
-- Indexes for table `editorpicks_comment`
--
ALTER TABLE `editorpicks_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_f_key` (`auth_id`);

--
-- Indexes for table `newcomer`
--
ALTER TABLE `newcomer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `authorId` (`auth_id`);

--
-- Indexes for table `news_comment`
--
ALTER TABLE `news_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opinion`
--
ALTER TABLE `opinion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trailer`
--
ALTER TABLE `trailer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movieId` (`movie_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `blogs_comment`
--
ALTER TABLE `blogs_comment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `editorpicks`
--
ALTER TABLE `editorpicks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `editorpicks_comment`
--
ALTER TABLE `editorpicks_comment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `newcomer`
--
ALTER TABLE `newcomer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `news_comment`
--
ALTER TABLE `news_comment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `opinion`
--
ALTER TABLE `opinion`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `trailer`
--
ALTER TABLE `trailer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `fk_user_blog` FOREIGN KEY (`auth_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `editorpicks`
--
ALTER TABLE `editorpicks`
  ADD CONSTRAINT `fk_user_editorpick` FOREIGN KEY (`auth_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `movies`
--
ALTER TABLE `movies`
  ADD CONSTRAINT `fk_user_movie_update` FOREIGN KEY (`auth_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `fk_user_news` FOREIGN KEY (`auth_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `trailer`
--
ALTER TABLE `trailer`
  ADD CONSTRAINT `fk_movie_trailer` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
