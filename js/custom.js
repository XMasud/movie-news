// Javascript start

// javascript for dat
var d = new Date();
var curr_hour = d.getHours();
var cur_minits = d.getMinutes();
var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var days1 = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
if (curr_hour < 12) {
	AM_PM_Variable = "AM";
} else {
	AM_PM_Variable = "PM";
}
if (curr_hour == 0) {
	curr_hour = 12;
}
if (curr_hour > 12) {
	curr_hour = curr_hour - 12;
}

document.getElementById("demo4").innerHTML = curr_hour + ":" + cur_minits + " " + AM_PM_Variable;
document.getElementById("demo").innerHTML = days[d.getDay()];
document.getElementById("demo1").innerHTML = days1[d.getMonth()];
document.getElementById("demo2").innerHTML = d.getDate();
document.getElementById("demo3").innerHTML = d.getFullYear();
//document.getElementById("demo4").innerHTML = d.getHours();
//document.getElementById("demo5").innerHTML = d.getMinutes();



// form validation 
// login validation
function loginvalid() {
	var lname = document.getElementById("e_uname");
	var lpass = document.getElementById("lgn_pass");

	if (lname.value == "") {
		document.getElementById("login_username").innerHTML = "Please type a valide username!";
		return false;
	}
	if (lpass.value == "") {
		document.getElementById("login_pass").innerHTML = "Please type a valide password!";
		return false;
	}
}
// registration validation
function regvalid() {
	var mail = document.getElementById("r_email");
	var regusername = document.getElementById("username");
	var pass = document.getElementById("rpass");
	var repass = document.getElementById("rrpass");

	if (mail.value == "") {
		document.getElementById("reg_email").innerHTML = "please type a valide email address!";
		return false;
	}
	if (regusername.value == "") {
		document.getElementById("reg_username").innerHTML = "This username isn't available!";
		return false;
	}
	if (pass.value == "") {
		document.getElementById("reg_pass").innerHTML = "Please type a valide password!";
		return false;
	}
	if (pass.value != repass.value) {
		document.getElementById("reg_re_pass").innerHTML = "please type a valide password!";
		return false;
	}

}


// Javascript end



// Jquiry
(function () {
	"use stict";


	// banner slider start
	$('.sl').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
//		fade: true,
		autoplay: true,
		autoplaySpeed: 4000,
//		asNavFor: '.it'
	});
//	$('.it').slick({
//		slidesToShow: 5,
//		slidesToScroll: 1,
//		asNavFor: '.sl',
//		dots: false,
//		centerMode: true,
//		focusOnSelect: true
//	});
	// banner slider end
	// home gallery photo slider start
	$('.home_gphoto').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
	});
	// trailers slider start
	$('.trailersld').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
	});
	// trailers slider end
	// letest upcoming movie slider start
	$('.upcoming').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
	});
	// letest upcoming movie slider end
	// search jquery 
	$(document).ready(function () {
		$(".clinp").click(function () {
			$(".srinp").animate({
				width: 'toggle',
			});
		});
	});
	// editors's pick slider start
	$('.editors').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		vertical: true,
		verticalSwiping: true,
		infinite: true,
		autoplay: true,
		arrows: false,
        dots: false,
		autoplaySpeed: 10000
	});
	
	
	
	// top stories slider start
	$('.story').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		vertical: true,
		verticalSwiping: true,
		autoplay: true,
		arrows: false,
		touchMove: true,
        dots: false,
		autoplaySpeed: 10000
	}); 
	
	
	// back to top
	$(document).ready(function () {

		$offset = 500;

		$(".bttop").click(function () {

			$("html,body").animate({
				scrollTop: 0
			}, 500)

		});

	});
	// news page auto load
	$(".news_load").slice(0, 12).show();
	$("#next_load").on('click', function (e) {
		e.preventDefault();

		$(".news_load:hidden").slice(0, 6).slideDown();
		if ($(".news_load:hidden").length == 0) {
			$("#load").fadeOut('show');
		}
		$("html,body").animate({
			scrollTop: $(this).offset().top
		}, 1500)
	})
	// latest Movies auto load
	$(".itm_hide").slice(0, 8).show();
	$("#n_load").on('click', function (e) {
		e.preventDefault();

		$(".itm_hide:hidden").slice(0, 8).slideDown();
		if ($(".itm_hide:hidden").length == 0) {
			$("#load").fadeOut('show');
		}
		$("html,body").animate({
			scrollTop: $(this).offset().top
		}, 1500)
	})
	// upcoming Movies auto load
	$(".com_hide").slice(0, 8).show();
	$("#com_load").on('click', function (e) {
		e.preventDefault();

		$(".com_hide:hidden").slice(0, 8).slideDown();
		if ($(".com_hide:hidden").length == 0) {
			$("#load").fadeOut('show');
		}
		$("html,body").animate({
			scrollTop: $(this).offset().top
		}, 1500)
	})
	// gallery photo auto load
	$(".photo_hide").slice(0, 6).show();
	$("#photo_load").on('click', function (e) {
		e.preventDefault();

		$(".photo_hide:hidden").slice(0, 6).slideDown();
		if ($(".photo_hide:hidden").length == 0) {
			$("#load").fadeOut('show');
		}
		$("html,body").animate({
			scrollTop: $(this).offset().top
		}, 1500)
	})
	// gallery video auto load
	$(".video_hide").slice(0, 6).show();
	$("#vd_load").on('click', function (e) {
		e.preventDefault();

		$(".video_hide:hidden").slice(0, 6).slideDown();
		if ($(".video_hide:hidden").length == 0) {
			$("#load").fadeOut('show');
		}
		$("html,body").animate({
			scrollTop: $(this).offset().top
		}, 1500)
	})
	// blog auto load
	$(".blog_hide").slice(0, 8).show();
	$("#blog_load").on('click', function (e) {
		e.preventDefault();

		$(".blog_hide:hidden").slice(0, 6).slideDown();
		if ($(".blog_hide:hidden").length == 0) {
			$("#load").fadeOut('show');
		}
		$("html,body").animate({
			scrollTop: $(this).offset().top
		}, 1500)
	})
	// menu active js
	$(document).ready(function () {
		var path = window.location.pathname.split("/").pop();
		if (path == '') {
			path = 'index.html';
		}
		var target = $('.menu_item a[href="' + path + '"]');
		target.addClass('active');
	});

})(jQuery);

// lightbox for individual
MediumLightbox('figure.zoom-effect', {
	//margin:300
});
