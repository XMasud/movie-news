<?php
	session_start();
	
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "movienews24";
	
	$conn = mysqli_connect($servername,$username,$password,$dbname);
	
	$query = strip_tags(mysqli_real_escape_string($conn, $_POST['search']));
	//echo $search = $_POST['search'];
	if($query){
		
		$sql = "SELECT * FROM movies WHERE moviename LIKE '%".$query."%'";
		$result = mysqli_query($conn, $sql) or die (mysqli_error($conn));;
		if (!$result) {
			echo 'MySQL Error: ' . mysqli_error();
			exit;
		}else{
			
			if (mysqli_num_rows($result) > 0) {
				while($row = mysqli_fetch_assoc($result))
				{	
					header('location: movie_individual.php?id='.$row['id']);			
				}
			}else{
				header('location: index.php');
			}
		}
	}else{
		header('location: index.php');
	}

	
	
?>
