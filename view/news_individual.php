<?php
	session_start();
	include("../admin/connect.php");

	$newsId = $_GET['id'];

	$record = "SELECT * FROM `news` WHERE id ='$newsId' ";
  $connection = mysqli_query($conn, $record);
  $data = mysqli_fetch_array($connection);
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Movie News 24 || News Individual</title>
	<link rel="icon" href="../images/favicon.png">
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/slick.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/lightbox.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/responsive.css">

	<script src="../js/jquery.min.js"></script>

	<script type="text/javascript">

            $(document).ready(function () {

            $('#register-btn').click(function (e) {

          	var comment = $('#comment').val();
						var userId = $('#user_id').val();
						var newsId = $('#news_id').val();

						$.ajax({

							url: 'newsComment.php',
							type: 'POST',
							data:{
								"flag" : 1,
								"comment":comment,
								"userId" :userId,
								"newsId" :newsId
							},
							success: function(data){

								$('#comment').val("");
								console.log(data);
								$("#comment_item").load(location.href + " #comment_item");
							}



						});
        });
});
</script>




</head>

<body>
	<script src="../js/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			$('meta[name="viewport"]').prop('content', 'width=1280');
		});
	</script>
	<!--header area start-->
	<header id="full_header">
		<!--header date area start-->
		<div class="header_date_part container-fluid">
			<div class="container">
				<div class="col-sm-6">
					
				</div>
				<div id="register" class="col-sm-6 text-right">
					<?php
						$flag = 0;
						if(isset($_SESSION['type']) && $_SESSION['id']){

							if($_SESSION['type'] == "user" && $_SESSION['id'] != NULL){

								$flag = 1;
								$id = $_SESSION['id'];

								$sql = "SELECT * FROM `users` WHERE id='$id' ";

	             	$connection = mysqli_query($conn, $sql);
	              if (!$connection) {
	              die('Invalid query: ' . mysqli_error($conn));

	                }

	              while($row = mysqli_fetch_array($connection))
	              {
	                $name = $row['username'];
									$userId = $row['id'];
	                //echo $row['username'];
	              }

								?>
									<a href=""><?php echo $name;?></a> |
									<a href="../admin/logout.php">Logout</a>
								<?php

							}
							?>
							<?php
						}
						else{
							?>
							<a href="login.html">Login</a> |
							<a href="login.html">Register</a>

							<?php
						}

					?>


				</div>
			</div>
		</div>
		<!--header date area end-->
		<!--header logo area start-->
		<div class="header_logo_part container-fluid">
			<div class="container">
				<div class="col-sm-4" id="logo_part">
					<a href="index.php">
						<img src="../images/Logo_top.png" class="img-responsive" alt="">
					</a>
				</div>
                <div class="col-sm-8">
                    <form method="post" action="search.php">
                        <div class="search_bar text-right" id="search_bar">

                            <input type="text" name="search" class="srinp" placeholder="Search" /><i class="fa fa-search clinp"></i>
                            <button type="submit" style="/*float: right!important;*/ margin-left: 752px;" class="nav navbar-nav">Search</button>

                        </div>
                    </form>
                </div>
			</div>
			<div class="container">
					<div class="col-sm-12">
						<h5 class="text-left dat_time">
							<span id="demo"></span>, <span id="demo1"></span>, <span id="demo2"></span>, <span id="demo3"></span> | <span id="demo4"></span>
						</h5>
					</div>
				</div>
		</div>
		<!--header logo area end-->
		<!--header menu area start-->
		<div class="header_menu_part container-fluid">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
						</div>
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav menu_item" id="menu_bar">
								<li class=""><a href="index.php">হোম <span class="sr-only">(current)</span></a></li>
								<li><a href="movies.php">সিনেমা</a></li>
								<li><a href="news.php">সংবাদ</a></li>
								<li><a href="gallery.php">গ্যালারি</a></li>
								<li><a href="blog.php">ব্লগ</a></li>
								<li><a href="editor's_pick.php">সম্পাদকীয়</a></li>
								<li><a href="new_face.php">নতুন মুখ</a></li>
							</ul>


							<ul class="nav navbar-nav navbar-right" id="menu_icon">
								<li>
									<a href="#" target="_blank"><i class="fa fa-facebook-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-twitter-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-google-plus-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-linkedin-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
								</li>
							</ul>
							<ul class="nav navbar-nav navbar-right" id="nav_button">
									<li><a href="report.php">লিখে ফেলুন</a></li>
									<li><a href="register.php">রেজিস্ট্রেশন</a></li>
								</ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<!--header menu area end-->
	</header>
	<!--haeder area end-->
	<!--container area start-->
	<section id="full_container">
		<div class="container">
			<!--main container area start-->
			<div class="main_container news_container col-sm-9">
				<!--movie news area start-->
				<div class="news_full" id="news_full">

					<figure class="zoom-effect">
						<div class="aspectRatioPlaceholder">
							<div class="" style="padding-bottom: 50%;"></div>
							<div id="news_full_img" class=""><img src="../admin/images_news/<?php echo $data['picture'];?>" class="img" data-width="900" data-height="900" alt=""></div>
						</div>
					</figure>

					<div id="news_full_heading">
						<h2><?php echo $data['title'];?></h2>
					</div>
					<div id="news_full_details">
						<p><?php echo $data['news'];?></p>
					</div>
				</div>
				<div class="news_share" id="news_share">
					<span>SHARE </span>
					<ul id="news_share_icon">
						<li><a href="#" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
						<li><a href="#" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
						<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>

				<div class="news_commend" id="news_commend">
					<h4>COMMENT</h4>

				</div>
				<?php
					if($flag == 1){

						?>
						<form action="" method="POST">



							<div class="form-group">
								<textarea style="width: 600px;" class="form-control" rows="3" cols="50" name="comment" id="comment"></textarea>

							</div>

							<div class="form-group">
								<input type="hidden" name="user_id" id="user_id"  value="<?php  echo $userId; ?>">
							</div>

							<div class="form-group">
								<input type="hidden" name="news_id" id="news_id" value="<?php  echo $newsId; ?>">
							</div>

							<div class="form-group">
							<button type="button" name="register-btn" class="btn btn-primary btn-md" id="register-btn">Comment</button>
						</div>

						</form>

						<?php
					}
					else{
						?>
						<a href="login.html"><h5 style="color:red;">You must login or signup to post comment</h5></a>
						<br><br>
						<?php
					}

				?>

				<div class="user_comments" id="user_comments">
					<div class="user_comments_items" id="user_comments_items">
						<div class="comment_item" id="comment_item">

							<?php

								$sql = "SELECT users.*,news_comment.* FROM users ,news_comment WHERE  users.id =news_comment.user_id AND news_comment.news_id = $newsId ORDER BY date DESC";

								$connection = mysqli_query($conn, $sql);
								if (!$connection) {
								die('Invalid query: ' . mysqli_error($conn));

									}

								while($row = mysqli_fetch_array($connection))
								{
									?>

							<h4><i class="fa fa-user-circle-o"></i> <span> <?php echo $row['username']; ?></span><?php echo date("F jS, Y", strtotime($row['date'])); ?></h4>
							<div class="comment_part">
								<p><?php echo $row['comment']; ?> </p>
							</div>

						<?php

					}
					 ?>

						</div>


					</div>
				</div>
				<!--movie news area end-->
			</div>
			<!--main container area end-->
			<!--sidebar area start-->
			<div class="sidebar col-sm-3" id="sidbar">
					<!--top stoise area start-->

					<!--top stoise area start-->
					<div class="storise" id="storise">
						<h2>শীর্ষ খবর</h2>
						<div class="story">

							<?php

							$sql = "SELECT * FROM `news`ORDER BY id DESC";

							$connection = mysqli_query($conn, $sql);
							if (!$connection) {
							die('Invalid query: ' . mysqli_error($conn));

								}

							while($row = mysqli_fetch_array($connection))
							{
								?>




								<div class="storise_item">
									<h4>
										<?php echo substr($row['title'],0,30); ?>
									</h4>
									<p>
										<?php echo substr($row['news'],0,50); ?> </p>
									<h6><a href="news_individual.php?id=<?php echo $row['id']; ?>" target="_blank">Read more>></a></h6>
								</div>

								<?php
					}
						 ?>

						</div>
					</div>
					<!--top storise area end-->
					<!--box office area start-->
					<div class="box_office">
						<div class="bxoffice_cont">
							<h2>বক্স অফিসে চলছে!</h2>
							<div class="box_office_item" id="box_office_item">
								<ol id="office_item">
									<li><a href="#" target="_blank"><span>Dhaka Attack </span></a>BDT 1.2 Crore</li>
									<li><a href="#" target="_blank"><span>Dhaka Attack </span></a>BDT 1.2 Crore</li>
									<li><a href="#" target="_blank"><span>Dhaka Attack </span></a>BDT 1.2 Crore</li>
									<li><a href="#" target="_blank"><span>Dhaka Attack </span></a>BDT 1.2 Crore</li>
									<li><a href="#" target="_blank"><span>Dhaka Attack </span></a>BDT 1.2 Crore</li>
								</ol>
							</div>
						</div>
					</div>
					<!--box office area end-->
					<!--editor's pick area start-->

					<!--editor's pick area end-->



					<div class="editors_cont" id="editors_cont">
						<h2>সম্পাদকীয়</h2>
						<div class="editors">
							<?php

					$sql = "SELECT * FROM `editorpicks` ORDER BY id DESC";

					$connection = mysqli_query($conn, $sql);
					if (!$connection) {
					die('Invalid query: ' . mysqli_error($conn));

						}

					while($row = mysqli_fetch_array($connection))
					{
						?>

								<div class="editors_item" id="editors_item">
									<div class="editors_image" id="editors_image"><img src="../admin/images_editor/<?php echo $row['picture'];?>" alt=""></div>
									<div class="editors_details" id="editors_details">
										<h4>
											<?php echo substr($row['title'],0,30) ." ......"; ?>
										</h4>
										<h6><a href="editor's_individual.php?id=<?php echo $row['id']; ?>" target="_blank">Read More>></a></h6>
									</div>
									<div class="clr"></div>
								</div>


								<?php
				}
				?>


						</div>
					</div>
					<!--				-->
				</div>
			<!--sidebar area end-->
		</div>
	</section>
	<!--container area end-->
	<!--footer area start-->
	<footer id="full_footer">
		<!--footer1 area start-->
		<div class="footer1">
			<div class="bttop"><img src="../images/arrow_top.png" alt=""></div>
			<div id="footer_sidebar" class="container">
				<div class="col-md-2 col-sm-2">
					<a href="#" target="_blank"><img src="../images/Logo_bottom.png" class="img-responsive" alt=""></a>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank">হোমপেজ</a></li>
						<li><a href="#" target="_blank">ট্রেইলারসমূহ</a></li>
						<li><a href="#" target="_blank">রিভিউ</a></li>
						<li><a href="#" target="_blank">সিনেমাসমূহ</a></li>
						<li><a href="#" target="_blank">সংবাদ</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank">সার্চ করুন</a></li>
						<li><a href="#" target="_blank">গ্যালারি</a></li>
						<li><a href="#" target="_blank">শীর্ষ খবর</a></li>
						<li><a href="#" target="_blank">বক্স অফিস শীর্ষ তালিকা!</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank">ব্লগ</a></li>
						<li><a href="#" target="_blank">সম্পাদকীয়</a></li>
						<li><a href="#" target="_blank">আমাদের সম্বন্ধে</a></li>
						<li><a href="#" target="_blank">টার্মস এবং কন্ডিশন্স</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank"><i class="fa fa-facebook-square"></i> facebook</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-twitter-square"></i> twitter</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-google-plus-square"></i> google+</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank"><i class="fa fa-linkedin-square"></i> linkedin</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-instagram"></i> instagram</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!--footer1 area end-->
		<!--footer2 area start-->
		<div class="footer2">
			<div class="container">
				<div class="col-sm-6">
					<div id="change_copyright">
						<h5 class="text1 text-left"> &copy; MovieNews24.com 2017, All Rights Reserved</h5>
					</div>
				</div>
				<div class="col-sm-6">
					<div id="made_by">
						<h5 class="text2 text-right">ডিজাইন করেছেন ফুয়াদ সারোয়ার</h5>
					</div>
				</div>
			</div>
		</div>
		<!--footer2 area end-->
	</footer>
	<!--footer area end-->



	<script src="../js/jquery-1.12.4.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/slick.js"></script>
	<script src="../js/medium-lightbox.js"></script>
	<script src="../js/custom.js"></script>
	<script>


	</script>
</body>

</html>
