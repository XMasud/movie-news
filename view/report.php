<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Movie News 24 || Login</title>
	<link rel="icon" href="../images/favicon.png">
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/slick.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/responsive.css">
</head>

<body>
	<script src="../js/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			$('meta[name="viewport"]').prop('content', 'width=1280');
		});



	</script>
	<!--header area start-->
	<header id="full_header">
		<!--header date area start-->
		<div class="header_date_part container-fluid">
			<div class="container">
				<div class="col-sm-6">
					
				</div>
				<div id="register" class="col-sm-6 text-right">
					<a href="login.html">Login</a> |
					<a href="login.html">Register</a>
				</div>
			</div>
		</div>
		<!--header date area end-->
		<!--header logo area start-->
		<div class="header_logo_part container-fluid">
			<div class="container">
				<div class="col-sm-4" id="logo_part">
					<a href="index.php">
						<img src="../images/Logo_top.png" class="img-responsive" alt="">
					</a>
				</div>
                <div class="col-sm-8">
                    <form method="post" action="search.php">
                        <div class="search_bar text-right" id="search_bar">

                            <input type="text" name="search" class="srinp" placeholder="Search" /><i class="fa fa-search clinp"></i>
                            <button type="submit" style="/*float: right!important;*/ margin-left: 752px;" class="nav navbar-nav">Search</button>

                        </div>
                    </form>
                </div>
			</div>
			<div class="container">
					<div class="col-sm-12">
						<h5 class="text-left dat_time">
							<span id="demo"></span>, <span id="demo1"></span>, <span id="demo2"></span>, <span id="demo3"></span> | <span id="demo4"></span>
						</h5>
					</div>
				</div>
		</div>
		<!--header logo area end-->
		<!--header menu area start-->
		<div class="header_menu_part container-fluid">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
						</div>
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav menu_item" id="menu_bar">
									<li class=""><a href="index.php">হোম <span class="sr-only">(current)</span></a></li>
									<li><a href="movies.php">সিনেমা</a></li>
									<li><a href="news.php">সংবাদ</a></li>
									<li><a href="gallery.php">গ্যালারি</a></li>
									<li><a href="blog.php">ব্লগ</a></li>
									<li><a href="editor's_pick.php">সম্পাদকীয়</a></li>
									<li><a href="new_face.php">নতুন মুখ</a></li>
								</ul>


							<ul class="nav navbar-nav navbar-right" id="menu_icon">
								<li>
									<a href="#" target="_blank"><i class="fa fa-facebook-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-twitter-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-google-plus-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-linkedin-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
								</li>
							</ul>
							<ul class="nav navbar-nav navbar-right" id="nav_button">
									<li><a href="report.php">লিখে ফেলুন</a></li>
									<li><a href="register.php">রেজিস্ট্রেশন</a></li>
								</ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<!--header menu area end-->
	</header>
	<!--haeder area end-->
	<!--container area start-->
	<section id="full_container">
		<div class="container">
			<!--registation area start-->
			<div class="col-sm-12 col-md-12">
				<div class="reg_part" id="reg_part">
					<h2>আমাদের পোর্টাল সম্পর্কে আপনার মতামত লিখুন</h2>
					<form action="saveReport.php" id="opinionForm" method="POST">
						<label for="name">নাম</label>
						<input type="text" id="name" name="name" class="require" placeholder="নাম লিখুন"><br>
						<label for="r_email">ই-মেইল</label>
						<input type="email" id="r_email" name="email" class="require" placeholder="example@domain.com"><br>
						<label class="text_area" for="text">আপনার বার্তা লিখুন</label>
						<textarea type="text" name="message" class="require" id="text" cols="50" rows="" placeholder="Text Here......"></textarea>
						<br>
						<button type="button" name="submit-report" id="submit-report">পাঠিয়ে দিন</button>
					</form>
				</div>
			</div>
			<!--registation area end-->
		</div>
	</section>
	<!--container area end-->
	<!--footer area start-->
	<footer id="full_footer">
		<!--footer1 area start-->
		<div class="footer1">
			<div class="bttop"><img src="../images/arrow_top.png" alt=""></div>
			<div id="footer_sidebar" class="container">
				<div class="col-md-2 col-sm-2">
					<a href="index.php" target="_blank"><img src="../images/Logo_bottom.png" class="img-responsive" alt=""></a>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank">হোমপেজ</a></li>
						<li><a href="#" target="_blank">ট্রেইলারসমূহ</a></li>
						<li><a href="#" target="_blank">রিভিউ</a></li>
						<li><a href="#" target="_blank">সিনেমাসমূহ</a></li>
						<li><a href="#" target="_blank">সংবাদ</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank">সার্চ করুন</a></li>
						<li><a href="#" target="_blank">গ্যালারি</a></li>
						<li><a href="#" target="_blank">শীর্ষ খবর</a></li>
						<li><a href="#" target="_blank">বক্স অফিস শীর্ষ তালিকা!</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank">ব্লগ</a></li>
						<li><a href="#" target="_blank">সম্পাদকীয়</a></li>
						<li><a href="#" target="_blank">আমাদের সম্বন্ধে</a></li>
						<li><a href="#" target="_blank">টার্মস এবং কন্ডিশন্স</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank"><i class="fa fa-facebook-square"></i> facebook</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-twitter-square"></i> twitter</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-google-plus-square"></i> google+</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank"><i class="fa fa-linkedin-square"></i> linkedin</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-instagram"></i> instagram</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!--footer1 area end-->
		<!--footer2 area start-->
		<div class="footer2">
			<div class="container">
				<div class="col-sm-6">
					<div id="change_copyright">
						<h5 class="text1 text-left"> &copy; MovieNews24.com 2017, All Rights Reserved</h5>
					</div>
				</div>
				<div class="col-sm-6">
					<div id="made_by">
						<h5 class="text2 text-right">ডিজাইন করেছেন ফুয়াদ সারোয়ার</h5>
					</div>
				</div>
			</div>
		</div>
		<!--footer2 area end-->
	</footer>
	<!--footer area end-->

    <script>
        $('#submit-report').click(function () {

            var isValid = true;
            $('.require').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });

            if (isValid == false)
                e.preventDefault();
            else{
                $('#opinionForm').submit();
                //alert(1);
            }
        })

        $('.require').click(function () {

            $(this).css({
                "border": "",
                "background": ""
            });

        })

    </script>

	<script src="../js/jquery-1.12.4.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/slick.js"></script>
	<script src="../js/custom.js"></script>
</body>

</html>
