<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Movie News 24 || Login</title>
	<link rel="icon" href="../images/favicon.png">
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/slick.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/responsive.css">
</head>

<body>
	<script src="../js/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			$('meta[name="viewport"]').prop('content', 'width=1280');
		});



	</script>
	<!--header area start-->
	<header id="full_header">
		<!--header date area start-->
		<div class="header_date_part container-fluid">
			<div class="container">
				<div class="col-sm-6">
					
				</div>
				<div id="register" class="col-sm-6 text-right">
					<a href="login.html">Login</a> |
					<a href="login.html">Register</a>
				</div>
			</div>
		</div>
		<!--header date area end-->
		<!--header logo area start-->
		<div class="header_logo_part container-fluid">
			<div class="container">
				<div class="col-sm-4" id="logo_part">
					<a href="index.php">
						<img src="../images/Logo_top.png" class="img-responsive" alt="">
					</a>
				</div>
                <div class="col-sm-8">
                    <form method="post" action="search.php">
                        <div class="search_bar text-right" id="search_bar">

                            <input type="text" name="search" class="srinp" placeholder="Search" /><i class="fa fa-search clinp"></i>
                            <button type="submit" style="/*float: right!important;*/ margin-left: 752px;" class="nav navbar-nav">Search</button>

                        </div>
                    </form>
                </div>
			</div>
			<div class="container">
					<div class="col-sm-12">
						<h5 class="text-left dat_time">
							<span id="demo"></span>, <span id="demo1"></span>, <span id="demo2"></span>, <span id="demo3"></span> | <span id="demo4"></span>
						</h5>
					</div>
				</div>
		</div>
		<!--header logo area end-->
		<!--header menu area start-->
		<div class="header_menu_part container-fluid">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
						</div>
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav menu_item" id="menu_bar">
									<li class=""><a href="index.php">হোম <span class="sr-only">(current)</span></a></li>
									<li><a href="movies.php">সিনেমা</a></li>
									<li><a href="news.php">সংবাদ</a></li>
									<li><a href="gallery.php">গ্যালারি</a></li>
									<li><a href="blog.php">ব্লগ</a></li>
									<li><a href="editor's_pick.php">সম্পাদকীয়</a></li>
									<li><a href="new_face.php">নতুন মুখ</a></li>
								</ul>


							<ul class="nav navbar-nav navbar-right" id="menu_icon">
								<li>
									<a href="#" target="_blank"><i class="fa fa-facebook-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-twitter-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-google-plus-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-linkedin-square"></i></a>
								</li>
								<li>
									<a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
								</li>
							</ul>
							<ul class="nav navbar-nav navbar-right" id="nav_button">
									<li><a href="report.php">লিখে ফেলুন</a></li>
									<li><a href="register.php">রেজিস্ট্রেশন</a></li>
								</ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
		<!--header menu area end-->
	</header>
	<!--haeder area end-->
	<!--container area start-->
	<section id="full_container">
		<div class="container">
			<!--registation area start-->
			<div class="col-sm-12 col-md-12">
				<div class="registration_part" id="reg_part">
					<h2>ব্যাক্তিগত তথ্য</h2>
					<form id="registerForm" action="saveRegister.php" enctype="multipart/form-data" method="post">
						<label for="name">নাম</label>
						<input type="text" class="require" name="name" placeholder="এখানে আপনার নাম লিখুন"><br>
						<label for="birth">জন্ম তারিখ</label>
						<input type="text" class="require" name="birth_date" placeholder="এখানে আপনার জন্ম তারিখ লিখুন"><br>
						<label for="lang">ভাষা</label>
						<input type="text" class="require" name="language" placeholder="আপনি কি কি ভাষা জানেন ? "><br>
						<label for="height">উচ্চতা </label>
						<input type="text" class="require" name="height" placeholder="এখানে লিখুন"><br>
						<label for="weight">ওজন</label>
						<input type="text" class="require" name="weight" placeholder="আপনার উজন"><br>
						<label for="body_color">গায়ের রং</label>
						<input type="text" class="require" name="color" placeholder="আপনার গায়ের রং"><br>
						<label for="religion">ধর্ম</label>
						<input type="text" class="require" name="religion" placeholder="আপনার ধর্ম"><br>
						<label for="married">বৈবাহিক অবস্থা</label>
						<input name="marriage"  type="checkbox" id="married" value="Married"> বিবাহিত
						<input name="marriage"  type="checkbox" id="unmarried" value="Unmarried"> অবিবাহিত<br>
						<label for="gender">লিঙ্গ</label>
						<input name="gander" class="check" type="checkbox" id="gender" value="Male"> পুরুষ
						<input name="gander" class="check" type="checkbox" id="gender" value="Female"> নারী
						<input name="gander" class="check" type="checkbox" id="gender" value="Other"> অন্যান্য
					<h2>যোগাযোগের তথ্য</h2>
						<label for="phone">ফোন নাম্বার</label>
						<input type="text" class="require" name="phone_number" placeholder="আপনার ফোন নাম্বার"><br>
						<label for="fb">ফেইসবুক</label>
						<input type="text" class="require" name="facebook" placeholder="আপনার ফেইসবুক ঠিকানা"><br>
						<label for="email">ইমেইল</label>
						<input type="email" class="require" name="email" placeholder="আপনার ইমেইল এড্রেস"><br>
						<label for="address1">স্থায়ী ঠিকানা</label>
						<input type="text" class="require" name="present_address" placeholder="আপনার স্থায়ী ঠিকানা লিখুন"><br>
						<label for="address2">বর্তমান ঠিকানা</label>
						<input type="text" class="require" name="permanent_address" placeholder="আপনার বর্তমান ঠিকানা লিখুন">
					<h2>শিক্ষাগত যোগ্যতা</h2>
						<label for="elementary">মাধ্যমিক</label>
						<input type="text" class="require" name="high_school" placeholder="বিদ্যালয়ের নাম">
						 পাশের সাল <input type="text" class="require" name="high_school_pass" placeholder="শিক্ষা সাল"><br>
						 <label for="school">উচ্চ মাধ্যমিক</label>
						<input type="text" class="require" name="college" placeholder="কলেজের নাম">
				        পাশের সাল <input type="text" class="require" name="college_pass" placeholder="শিক্ষা সাল"><br>
						 <label for="college">স্নাতক</label>
						<input type="text" class="require" name="honors" placeholder="কলেজ/বিশ্ববিদ্যালয়ের নাম">
						 পাশের সাল <input type="text" class="require" name="honors_pass" placeholder="শিক্ষা সাল"><br>
						<label for="degree">স্নাতকোত্তর</label>
						<input type="text" class="require" name="masters" placeholder="ডিগ্রী প্রদানকারী প্রতিষ্ঠানের নাম"><br>
						<label for="special">অন্যান্য</label>
						<input type="text" class="require" name="another" placeholder="অন্যান্য শিক্ষাগত যোগ্যতা যা আপনি উল্লেখ করতে চান" ><br>
						
					<h2>পারিবারিক তথ্য</h2>
						<label for="fname">পিতার নাম</label>
						<input type="text" class="require" name="fathers_name" placeholder="আপনার পিতার নাম"><br>
						<label for="fpassion">পিতার পেশা</label>
						<input type="text" class="require" name="fathers_occupation" placeholder="আপনার পিতার পেশা"><br>
						<label for="mname">মাতার নাম</label>
						<input type="text" class="require" name="mothers_name" placeholder="আপনার মাতার নাম"><br>
						<label for="mpassion">মাতার পেশা</label>
						<input type="text" class="require" name="mothers_occupation" placeholder="আপনার মাতার পেশা"><br>
						<label for="spassion">সহোদর</label>
						<input type="text" class="require" name="sibling" placeholder="আপনার ভাই/বোন"><br>
						<label for="sassion"></label>
						<input type="text" class="require" id="sname2" placeholder="আপনার ভাই/বোন"><br>
						<label for="member">সদস্য সংখ্যা</label>
						<input type="text" class="require" name="total_number" placeholder="পরিবারের সদস্য সংখ্যা"><br>
						
					<h2>পছন্দসমূহ</h2>
						<label for="habite">অভ্যাস</label>
						<input type="text" class="require" name="habit" placeholder="আপনার অভ্যাস"><br>
						<label for="hobby">শখ</label>
						<input type="text" class="require" name="hobby" placeholder="আপনার শখ"><br>
						<label for="gd_feel">যেসব বিষয়ে আগ্রহী</label>
						<input type="text" class="require" name="interested" placeholder="আপনার কি করতে ভাল লাগে ?"><br>
						
					<h2>পোর্টফোলিও যোগ করুন</h2>
						<label for="photo">ফটো</label>
						<input type="file" class="require" name="photo"><br>
						<label for="audio">অডিও</label>
						<input type="file" class="require" name="audio"><br>
						<label for="video">ভিডিও</label>
						<input type="file" class="require" name="video"><br>
                        <span id="checkboxError" style="color: red;display: none;">Please select above checkboxes...</span><br>
						<button id="submitBtn" >রেজিস্ট্রার করুন</button>
					</form>
				</div>
			</div>
			<!--registation area end-->
		</div>
	</section>
	<!--container area end-->
	<!--footer area start-->
	<footer id="full_footer">
		<!--footer1 area start-->
		<div class="footer1">
			<div class="bttop"><img src="../images/arrow_top.png" alt=""></div>
			<div id="footer_sidebar" class="container">
				<div class="col-md-2 col-sm-2">
					<a href="index.php" target="_blank"><img src="../images/Logo_bottom.png" class="img-responsive" alt=""></a>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank">হোমপেজ</a></li>
						<li><a href="#" target="_blank">ট্রেইলারসমূহ</a></li>
						<li><a href="#" target="_blank">রিভিউ</a></li>
						<li><a href="#" target="_blank">সিনেমাসমূহ</a></li>
						<li><a href="#" target="_blank">সংবাদ</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank">সার্চ করুন</a></li>
						<li><a href="#" target="_blank">গ্যালারি</a></li>
						<li><a href="#" target="_blank">শীর্ষ খবর</a></li>
						<li><a href="#" target="_blank">বক্স অফিস শীর্ষ তালিকা!</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank">ব্লগ</a></li>
						<li><a href="#" target="_blank">সম্পাদকীয়</a></li>
						<li><a href="#" target="_blank">আমাদের সম্বন্ধে</a></li>
						<li><a href="#" target="_blank">টার্মস এবং কন্ডিশন্স</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank"><i class="fa fa-facebook-square"></i> facebook</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-twitter-square"></i> twitter</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-google-plus-square"></i> google+</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2">
					<ul>
						<li><a href="#" target="_blank"><i class="fa fa-linkedin-square"></i> linkedin</a></li>
						<li><a href="#" target="_blank"><i class="fa fa-instagram"></i> instagram</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!--footer1 area end-->
		<!--footer2 area start-->
		<div class="footer2">
			<div class="container">
				<div class="col-sm-6">
					<div id="change_copyright">
						<h5 class="text1 text-left"> &copy; MovieNews24.com 2017, All Rights Reserved</h5>
					</div>
				</div>
				<div class="col-sm-6">
					<div id="made_by">
						<h5 class="text2 text-right">ডিজাইন করেছেন ফুয়াদ সারোয়ার</h5>
					</div>
				</div>
			</div>
		</div>
		<!--footer2 area end-->
	</footer>
	<!--footer area end-->



	<script src="../js/jquery-1.12.4.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/slick.js"></script>
	<script src="../js/custom.js"></script>
</body>

<script>
    $('#submitBtn').click(function (e) {


        var flag = 1;
        var isValid = true;
        $('.require').each(function () {
            if ($.trim($(this).val()) == '') {
                isValid = false; /* Required class style */
                $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }
            else { /* Required class style removed */
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });

        if(isValid == true){
            flag = 1;
        }else{
            e.preventDefault();
            flag = 2;
        }

        var marriage = $("input[name='marriage']").serializeArray();
        if (marriage.length === 0)
        {
            $('#checkboxError').css('display','block');
            e.preventDefault();
            flag = 2;

        }
        else
        {
            flag = 1;
            $('#checkboxError').css('display','none');
        }

        var gander = $("input[name='gander']").serializeArray();
        if (gander.length === 0)
        {

            $('#checkboxError').css('display','block');
            e.preventDefault();
            flag = 2;

        }
        else
        {
            flag = 1;
            $('#checkboxError').css('display','none');
        }

        if(flag == 1){
            $('#registerForm').submit();
        }

    });

    $('.require').click(function () {
        $(this).css({
            "border": "",
            "background": ""
        });
    });
</script>

</html>
